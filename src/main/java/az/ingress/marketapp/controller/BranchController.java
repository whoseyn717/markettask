package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.BranchDto;
import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.MarketDto;
import az.ingress.marketapp.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping("/branch")
    public BranchDto create(@PathVariable Long marketId,
                       @RequestBody CreateBranchDto branchDto) {
        return branchService.create(marketId, branchDto);
    }
    @GetMapping("/branch")
    public List<BranchDto> getAll(@PathVariable Long marketId) {
        return branchService.findAllbyMarketId(marketId);
    }
@PutMapping("/market/{marketId}/branch/{branchId}")
    public BranchDto updateBranch(@PathVariable Long marketId,
                                  @PathVariable Long branchId,
                                  @RequestBody CreateBranchDto branchDto){
        return branchService.updateBranch(marketId,branchId,branchDto);
}
@DeleteMapping("/{marketId}/{branchId}")
public void deleteBranch(@PathVariable Long marketId, @PathVariable Long branchId){
        branchService.deleteBranch(marketId,branchId);
}


}
