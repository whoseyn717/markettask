package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.MarketDto;
import az.ingress.marketapp.service.MarketService;
import jakarta.persistence.MapKey;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Valid
public class MarketController {

    private final MarketService marketService;

    @PostMapping("/market")
    public MarketDto create(@RequestBody CreateMarketDto marketDto) {
        return marketService.create(marketDto);
    }

    @GetMapping("/market")
    public List<MarketDto> all() {
        return marketService.findAll();
    }

    @GetMapping("/{id}")
    public MarketDto findById(@PathVariable Long id) {
        return marketService.findById(id);
    }

    @PutMapping("/market/{id}")
    public MarketDto update(@PathVariable Long id, @RequestBody CreateMarketDto marketDto) {
        return marketService.update(id, marketDto);
    }

    @DeleteMapping("/market/{id}")
    public void delete(@PathVariable Long id) {
        marketService.delete(id);
    }
}
