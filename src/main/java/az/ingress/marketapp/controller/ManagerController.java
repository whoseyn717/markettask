package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.dto.ManagerDto;
import az.ingress.marketapp.repository.genericSearch.SearchCriteria;
import az.ingress.marketapp.service.ManagerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Valid
public class ManagerController {

    private final ManagerService managerService;

    @PostMapping("/manager/{marketId}/{branchId}/")
    public ManagerDto create(@PathVariable Long marketId,
                             @PathVariable Long branchId,
                             @RequestBody CreateManagerDto managerDto) {
        return managerService.create(marketId,branchId,managerDto);
    }
    @GetMapping("/manager/{marketId}/{branchId}")
    public List<ManagerDto> findAllByMarketIdAndBranchId(@PathVariable Long marketId, @PathVariable Long branchId){
        return managerService.findAllByMarketIdAndBranchId(marketId,branchId);

    }
    @PutMapping("/manager/{marketId}/{branchId}/{managerId}")
    public ManagerDto update(@PathVariable Long marketId,
                             @PathVariable Long branchId,
                             @PathVariable Long managerId,
                             @RequestBody CreateManagerDto managerDto){
        return managerService.update(marketId,branchId,managerId,managerDto);
    }
    @DeleteMapping("/manager/{marketId}/{branchId}/{managerId}")
    public void delete(@PathVariable Long marketId,@PathVariable Long branchId,@PathVariable Long managerId){
        managerService.delete(managerId,branchId,managerId);}

    @PostMapping("/manager/search")
    public List<ManagerDto> searchByName(@RequestBody List<SearchCriteria> searchCriteria){
        return managerService.searchByName(searchCriteria);

    }


    }






