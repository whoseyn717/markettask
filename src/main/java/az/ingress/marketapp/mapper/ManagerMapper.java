package az.ingress.marketapp.mapper;


import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.ManagerDto;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.model.Market;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ManagerMapper {

    Manager dtoToManager(CreateManagerDto dto);
    ManagerDto managerToDto(Manager manager);
    List<ManagerDto> listToResponseDto(List<Manager> managers);

    List<ManagerDto> managerToDto(List<Manager> all);
}
