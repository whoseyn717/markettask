package az.ingress.marketapp.dto;

import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.model.Phone;
import jakarta.persistence.CascadeType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarketDto {
    Long id;
    String name;
    String address;
    List<BranchDto> branches;
    List<PhoneDto> phones;
    ManagerDto manager;
}
