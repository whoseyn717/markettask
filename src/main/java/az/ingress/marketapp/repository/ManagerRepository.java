package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Manager;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ManagerRepository extends JpaRepository<Manager, Long>, JpaSpecificationExecutor<Manager> {
    @EntityGraph(attributePaths = "phones", type = EntityGraph.EntityGraphType.FETCH)
    List<Manager> findAll();


    @EntityGraph(attributePaths = "phones",type = EntityGraph.EntityGraphType.FETCH)
    List<Manager> findAll(Specification<Manager> specification);




}
