package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Branch;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BranchRepository extends JpaRepository<Branch, Long> {
    @EntityGraph(attributePaths = "phones", type = EntityGraph.EntityGraphType.FETCH)
    Optional<List<Branch>> findAllByMarketId(Long marketId);
}
