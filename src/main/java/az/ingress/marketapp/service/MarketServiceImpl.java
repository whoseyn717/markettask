package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.MarketDto;
import az.ingress.marketapp.mapper.MarketMapper;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {
    private final MarketRepository marketRepository;
    private final MarketMapper marketMapper;

    @Override
    public MarketDto create(CreateMarketDto dto) {
        Optional<Market> byName = marketRepository.findByName(dto.getName());
        if (byName.isPresent()) {
            throw new RuntimeException("Name already exists");
        }
        Market market = marketMapper.dtoToMarket(dto);
       return marketMapper.marketToDto(marketRepository.save(market));
    }

    @Override
    public List<MarketDto> findAll() {
        //var market = marketRepository.findAll();
        return marketMapper.listMarketToDto(marketRepository.findAll());
    }

    @Override
    public MarketDto findById(Long id) {
        return marketMapper.marketToDto(marketRepository.findById(id).orElseThrow(()->new RuntimeException("Not found")));
    }

    @Override
    public MarketDto update(Long id, CreateMarketDto marketDto) {
        Market market = marketRepository.findById(id).orElseThrow(()->new RuntimeException("Not found"));
        market = marketMapper.dtoToMarket(marketDto);
        market.setId(id);
        return marketMapper.marketToDto(marketRepository.save(market));


    }

    @Override
    public void delete(Long id) {
        marketRepository.deleteById(id);
    }
}
