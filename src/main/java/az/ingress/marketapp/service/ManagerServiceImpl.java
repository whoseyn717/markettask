package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.dto.ManagerDto;
import az.ingress.marketapp.mapper.ManagerMapper;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.ManagerRepository;
import az.ingress.marketapp.repository.genericSearch.CustomSpecification;
import az.ingress.marketapp.repository.genericSearch.SearchCriteria;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private  BranchRepository branchRepository;
    private  ManagerRepository managerRepository;

    private CustomSpecification<Manager> customSpecification;

    private  ManagerMapper managerMapper;




    @Override
    public ManagerDto create(Long marketId, Long branchId, CreateManagerDto managerDto) {
        var branch = branchRepository.findById(marketId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.dtoToManager(managerDto);
        branch.setManager(manager);
        return managerMapper.managerToDto(managerRepository.save(manager));
    }

    @Override
    public List<ManagerDto> findAllByMarketIdAndBranchId(Long marketId, Long branchId) {
        return managerMapper.managerToDto(managerRepository.findAll());
    }

    @Override
    public ManagerDto update(Long marketId, Long branchId, Long managerId, CreateManagerDto managerDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.dtoToManager(managerDto);
        manager.setId(managerId);
        manager.setBranch(branch);
        return managerMapper.managerToDto(managerRepository.save(manager));

    }

    @Override
    public void delete(Long marketId, Long branchId, Long managerId) {
        managerRepository.deleteById(managerId);
    }

    @Override
    public List<ManagerDto> searchByName(List<SearchCriteria> searchCriteria) {
        CustomSpecification<Manager> managerSpecification = new CustomSpecification<>(searchCriteria);

        List<Manager> managers = managerRepository.findAll(managerSpecification);

        return managerMapper.listToResponseDto(managers);


    }
}