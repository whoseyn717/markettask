package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.BranchDto;
import az.ingress.marketapp.dto.CreateBranchDto;

import java.util.List;

public interface BranchService {
     BranchDto create(Long marketId, CreateBranchDto branchDto);
     BranchDto updateBranch(Long marketId, Long branchId, CreateBranchDto branchDto);
      void deleteBranch(Long marketId, Long id);
     List<BranchDto> findAllbyMarketId(Long marketId);
}
