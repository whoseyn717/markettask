package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.BranchDto;
import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.mapper.BranchMapper;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final BranchMapper branchMapper;

    @Override
    public BranchDto create(Long marketId, CreateBranchDto branchDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.dtoToBranch(branchDto);
        branch.setMarket(market);
        return branchMapper.BranchToDto(branchRepository.save(branch));
    }
    @Override
    public BranchDto updateBranch(Long marketId, Long branchId, CreateBranchDto branchDto) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException("Branch not found"));
        if (!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Request market id is not equals branch's market id");

        branch = branchMapper.dtoToBranch(branchDto);
        branch.setId(branchId);
       return branchMapper.BranchToDto(branchRepository.save(branch));
    }
    public void deleteBranch(Long marketId, Long id) {
        Branch branch = branchRepository.findById(id).orElseThrow(() -> new RuntimeException("Branch not found"));
        if (!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Request market id is not equals branch's market id");
        branchRepository.deleteById(id);
    }
    public List<BranchDto> findAllbyMarketId(Long marketId){
        var branches = branchRepository.findAllByMarketId(marketId).orElseThrow(()-> new RuntimeException("Branches not found"));
        return branchMapper.listBranchToDto(branches);
    }



}
