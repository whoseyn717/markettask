package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.dto.ManagerDto;
import az.ingress.marketapp.repository.genericSearch.SearchCriteria;

import java.util.List;

public interface ManagerService {
    ManagerDto create(Long marketId, Long branchId, CreateManagerDto managerDto);

    List<ManagerDto> findAllByMarketIdAndBranchId(Long marketId, Long branchId);

    ManagerDto update(Long marketId, Long branchId, Long managerId, CreateManagerDto managerDto);
    void delete(Long marketId,Long branchId,Long managerId);

    List<ManagerDto> searchByName(List<SearchCriteria> searchCriteria);

}



